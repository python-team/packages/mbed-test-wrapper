% MBED-TEST-WRAPPER(1) __VERSION__ | User Commands
% 
% December 2018

# NAME

**mbed-test-wrapper** - wrap the Mbed test loader to run tests on target hardware

# SYNOPSIS

| **mbed-test-wrapper** **\-\-target** _TARGET_ \[**\-\-timeout** _TIMEOUT_\] **program**

| **mbed-test-wrapper** **\-\-help**

# DESCRIPTION

**mbed-test-wrapper** wraps **mbedhtrun** (the Mbed test loader) so that it can be used by **yotta** targets to run tests on target hardware (e.g. K64F).

# OPTIONS

-t, \-\-target _TARGET_
:   execution target (e.g. K64F)

-i, \-\-timeout _TIMEOUT_
:   maximum time (seconds) to wait for program (default: 10)

program
:   executable file to run

-h, \-\-help
:   show usage details and exit

# REPORTING BUGS

Upstream bug tracker: https://github.com/autopulated/mbed-test-wrapper/issues

# COPYRIGHT

Copyright (c) 2015 ARM Limited

# AUTHOR

This manual page is based on the mbed-test-wrapper documentation. It was created by Nick Morrott <nickm@debian.org> for the Debian GNU/Linux system, but may be used by others

# SEE ALSO

**yotta**(1), **mbedhtrun**(1), **mbedls**(1)
